# M4th-group-scripts

## Prerequisites

* Anaconda3

## Install env

	conda create --name <env_name> numpy matplotlib jupyter scipy
	
## Sourcing env
    source activate <env_name>

## Starting jupyter

*In main project catalog*

    jupyter notebook
    
