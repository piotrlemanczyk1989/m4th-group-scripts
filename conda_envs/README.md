Export env to file
```sh
source activate <your_environment>
conda env export | grep -v "^prefix: " > environment.yml
```

Import env from file
conda env create -f environment.yml
